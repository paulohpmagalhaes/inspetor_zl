---
Sistema: Nível 2.5 CR
Status: Operacional
Endereço Produção: https://10.118.48.207
Endereço Desenvolvimento: https://10.72.25.184
Projeto: Inspetor ZL
Info: "Informações para interface entre QTS/Nível 2.5 com dispositivo móvel de apontamento de dados de qualidade"
---

# Inspetor ZL Markdown **(v2.0)**

Este Markdown tem como objetivo orientação para **interface do Sistemas QTS e Nível 2.5 com dispositivo móvel de apontamento de dados de qualidade**.

Serão mostrados neste documento:
- Endpoits que serão utilizados
- Estrutura básica das mensagens
- Descrição dos campos das mensagens
- Exemplos de mensagens completas
- Exemplo de comportamento dos campos conforme valor
- Sugestão de funcionamento para o aplicativo baseado no funcionamento do apontamento no QTS.Client

### Endpoits para a aplicação do dispositivo móvel

Segue abaixo a lista de endpoints, sua funcionalidade e parâmetros.

|   |Funcionalidade        | URL                                                | Método |
| - |----------------------| -------------------------------------------------- | ------ |
| 1 | **Recupera Postos**  | {host}/api/cr/apontamento/postos                   |  GET   |
| 2 | **Login**            | {host}/api/cr/apontamento/login/:posto/:matricula  |  POST  |
| 3 | **Logout**           | {host}/api/cr/apontamento/logout/:posto/:matricula |  POST  |
| 4 | **Recupera Defeitos**| {host}/api/cr/defeitos                             |  GET   |
| 5 | **Requisita dados**  | {host}/api/cr/apontamento/requisitadados/:posto    |  GET   |
| 6 | **Envia dados**      | {host}/api/cr/apontamento/enviadados/:posto        |  POST  |



### 1. Recupera Postos

**Funcionalidade**: Retorna json com dados de postos de trabalho disponíveis

* **URL:**  {host}/api/cr/apontamento/postos 

* **Método:**  `GET`
  
* **Parâmetros URL:**  `None` 

* **Parâmetros de Dados:**  `None`

* **Success Response:**  Code: 200, Content: `{ ['ZL1', 'ZL2', 'MSC1', 'MSC2'] }`
 
* **Error Response:**

  * **Code:** 500 Internal Server Error, Content: `{ error : "Error Message" }`

  * **Code:** 401 Unauthorized, Content: `{ error : "Access Denied." }`

* **Sample Call:**
````markdown
 $http.get('api/cr/apontamento/postos')
              .success(function (data) {
               console.log(data);
              })
````


### 2. Login Posto Qualidade

**Funcionalidade**: Vincula usuário logado ao posto especificado
**NÃO CONFUNDIR COM LOGIN "TEMPLATE" DO SHALYN

* **URL:**  {host}/api/cr/apontamento/loginqualidade

* **Método:**  `POST`
  
* **Parâmetros URL:**  `None`

* **Parâmetros de Dados:**  
````markdown
{
   user: $user,
   posto: $posto
}
````
 
* **Error Response:**

  * **Code:** 500 Internal Server Error, Content: `{ error : "Error Message" }`

  * **Code:** 401 Unauthorized, Content: `{ error : "Access Denied." }`

* **Sample Call:**
````markdown
 $http.post('api/cr/apontamento/login/MSC1/705521')
              .success(function (data) {
               console.log(data);
              })
````


### 3. Logout Posto Qualidade

**Funcionalidade**: Desvincula usuário logado ao posto especificado

* **URL:**  {host}/api/cr/apontamento/logoutqualidade

* **Método:**  `POST`
  
* **Parâmetros URL:**  `None`

* **Parâmetros de Dados:**  
````markdown
{
   user: $user,
   posto: $posto
}
````


* **Success Response:**  Code: 200, Content: `{ }`
 
* **Error Response:**

  * **Code:** 500 Internal Server Error, Content: `{ error : "Error Message" }`

  * **Code:** 401 Unauthorized, Content: `{ error : "Access Denied." }`

* **Sample Call:**
````markdown
 $http.post('api/cr/apontamento/logout/MSC1/705521')
              .success(function (data) {
               console.log(data);
              })
````

### 4. Recupera Defeitos

**Funcionalidade**: Retorna json com lista de defeitos para apontamento de laudo

* **URL:**  {host}/api/cr/defeitos

* **Método:**  `GET`
  
* **Parâmetros URL:**  `None` 

* **Parâmetros de Dados:**  `None`

* **Success Response:**  Code: 200, Content: 
````markdown
   {[{
      "versao":2,
      "codigo":"2585",
      "descricao":"RIR_Risco na rosca",
      "tipo":"I",
      "criado":"2021-03-27T17:04:04.307591-03:00"
   },
   {
      "versao":2,
      "codigo":"2586",
      "descricao":"RUGENC-Rug enc fora",
      "tipo":"I",
      "criado":"2021-03-27T17:04:04.307591-03:00"
   },]}
 ````

* **Error Response:**

  * **Code:** 500 Internal Server Error, Content: `{ error : "Error Message" }`

  * **Code:** 401 Unauthorized, Content: `{ error : "Access Denied." }`

* **Sample Call:**
````markdown
 $http.get('/api/cr/defeitos')
               .success(function (data) {
               console.log(data);
              })
````

### 5. Recupera Dados de Apontamento

**Funcionalidade**: Retorna json com todos os dados de qualidade do(s) tubo(s) no posto de trabalho em que o operador estiver logado

* **URL:**  {host}/api/cr/apontamento/requisitadados/:posto

* **Método:**  `GET`
  
* **Parâmetros URL:**  `posto=[string]` 

* **Parâmetros de Dados:**  `None`

* **Success Response:**  Code: 200, Content: 
````markdown
{
   "idPostoTrabalho":"MSC1",
   "idDispositivo":null,
   "matriculaInspetor":"705521",
   "tipoRosca":"VAM TOP HC",
   "laudosPossiveis":[
      "OK",
      "REJ"
   ],
   "valoresAtributo":[
      {
         "valor":1,
         "descricao":"OK"
      },
      {
         "valor":2,
         "descricao":"REJ"
      }
   ],
   "pendenciasGerais":null,
   "dadosApontamento":[
      {
         "ippn":188247800681,
         "laudo":"REJ",
         "codigoDefeito":"2122",
         "apontamentoPendente":true,
         "laudoPendente":true,
         "infDimPendente":true,
         "infVisPendente":false,
         "infDimForaFaixa":false,
         "infDimForaAlvo":false,
         "pendencias":[
            "Inf. dimensional \"009 - DiametroMinimoRosca\" deve ser apontada.",
            "Inf. dimensional \"008 - DiametroMaximoRosca\" deve ser apontada."
         ],
         "infDimensionais":[
            {
               "idcInfDimensional":31,
               "tag":8,
               "descricao":"DiametroSeloMedia",
               "unidade":"0,01 mm",
               "resolucao":1.0,
               "limiteInferior":-5.0,
               "alvoInferior":null,
               "alvoSuperior":null,
               "limiteSuperior":5.0,
               "valor":null,
               "aptPendente":true,
               "habilitada":true,
               "requerApontamento":true,
               "valorForaFaixa":false,
               "valorForaAlvo":false,
               "apontamentoAtributo":false,
               "apontamentoCalculado":true
            },
            {
               "idcInfDimensional":32,
               "tag":9,
               "descricao":"DiametroSeloOvalizacao",
               "unidade":"0,01 mm",
               "resolucao":1.0,
               "limiteInferior":0.0,
               "alvoInferior":null,
               "alvoSuperior":null,
               "limiteSuperior":9.0,
               "valor":null,
               "aptPendente":true,
               "habilitada":true,
               "requerApontamento":true,
               "valorForaFaixa":false,
               "valorForaAlvo":false,
               "apontamentoAtributo":false,
               "apontamentoCalculado":true
            }
         ],
         "infVisuais":[
            {
               "idcInfVisual":1,
               "tag":1,
               "descricao":"Visual",
               "valores":[
                  "OK",
                  "REJ"
               ],
               "valor":"OK",
               "habilitada":true,
               "reprovada":false
            },
            {
               "idcInfVisual":2,
               "tag":2,
               "descricao":"PenteRosca ",
               "valores":[
                  "OK",
                  "REJ"
               ],
               "valor":"OK",
               "habilitada":true,
               "reprovada":false
            }
         ]
      }
   ]
}
 ````

* **Error Response:**

  * **Code:** 500 Internal Server Error, Content: `{ error : "Error Message" }`

  * **Code:** 401 Unauthorized, Content: `{ error : "Access Denied." }`

* **Sample Call:**
````markdown
            $http.get('/api/cr/apontamento/requisitadados/MSC1')
               .success(function (data) {
               console.log(data);
              })
````

### 5. Envia Dados de Apontamento

**Funcionalidade**: Envia dados de apontamento de qualidade serializados em json, retornando os mesmos dados após crítica de dados no servidor

* **URL:**  {host}/api/cr/apontamento/enviadados/:posto 

* **Método:**  `GET`
  
* **Parâmetros URL:**  `posto=[string]` 

* **Parâmetros de Dados:**
````markdown
{
   "idPostoTrabalho":"MSC1",
   "idDispositivo":"ID do Dispositivo",
   "matriculaInspetor":"705521",
   "tipoRosca":"VAM TOP HC",
   "laudosPossiveis":null,
   "valoresAtributo":null,
   "pendenciasGerais":null,
   "dadosApontamento":[
      {
         "ippn":188247800681,
         "laudo":"REJ",
         "codigoDefeito":"2122",
         "apontamentoPendente":true,
         "laudoPendente":true,
         "infDimPendente":true,
         "infVisPendente":false,
         "infDimForaFaixa":false,
         "infDimForaAlvo":false,
         "pendencias":null,
         "infDimensionais":[
            {
               "idcInfDimensional":31,
               "tag":8,
               "descricao":"DiametroSeloMedia",
               "unidade":"0,01 mm",
               "resolucao":1.0,
               "limiteInferior":-5.0,
               "alvoInferior":null,
               "alvoSuperior":null,
               "limiteSuperior":5.0,
               "valor":4.5,
               "aptPendente":true,
               "habilitada":true,
               "requerApontamento":true,
               "valorForaFaixa":false,
               "valorForaAlvo":false,
               "apontamentoAtributo":false,
               "apontamentoCalculado":true
            },
            {
               "idcInfDimensional":32,
               "tag":9,
               "descricao":"DiametroSeloOvalizacao",
               "unidade":"0,01 mm",
               "resolucao":1.0,
               "limiteInferior":0.0,
               "alvoInferior":null,
               "alvoSuperior":null,
               "limiteSuperior":9.0,
               "valor":7.2,
               "aptPendente":true,
               "habilitada":true,
               "requerApontamento":true,
               "valorForaFaixa":false,
               "valorForaAlvo":false,
               "apontamentoAtributo":false,
               "apontamentoCalculado":true
            }
         ],
         "infVisuais":[
            {
               "idcInfVisual":1,
               "tag":1,
               "descricao":"Visual",
               "valores":[
                  "OK",
                  "REJ"
               ],
               "valor":"OK",
               "habilitada":true,
               "reprovada":false
            },
            {
               "idcInfVisual":2,
               "tag":2,
               "descricao":"PenteRosca ",
               "valores":[
                  "OK",
                  "REJ"
               ],
               "valor":"OK",
               "habilitada":true,
               "reprovada":false
            }
         ]
      }
   ]
}
 ````

* **Success Response:**  Code: 200, Content:
**Dados de Resposta são idênticos ao de envio, exceto a parte de crítica de dados em que o rastreamento pode "resetar" ou preencher dados e informações como "pendências"**
````markdown
{
   "idPostoTrabalho":"MSC1",
   "idDispositivo":null,
   "matriculaInspetor":"705521",
   "tipoRosca":"VAM TOP HC",
   "laudosPossiveis":[
      "OK",
      "REJ"
   ],
   "valoresAtributo":[
      {
         "valor":1,
         "descricao":"OK"
      },
      {
         "valor":2,
         "descricao":"REJ"
      }
   ],
   "pendenciasGerais":null,
   "dadosApontamento":[
      {
         "ippn":188247800681,
         "laudo":"REJ",
         "codigoDefeito":"2122",
         "apontamentoPendente":true,
         "laudoPendente":true,
         "infDimPendente":true,
         "infVisPendente":false,
         "infDimForaFaixa":false,
         "infDimForaAlvo":false,
         "pendencias":[
            "Inf. dimensional \"009 - DiametroMinimoRosca\" deve ser apontada.",
            "Inf. dimensional \"008 - DiametroMaximoRosca\" deve ser apontada."
         ],
         "infDimensionais":[
            {
               "idcInfDimensional":31,
               "tag":8,
               "descricao":"DiametroSeloMedia",
               "unidade":"0,01 mm",
               "resolucao":1.0,
               "limiteInferior":-5.0,
               "alvoInferior":null,
               "alvoSuperior":null,
               "limiteSuperior":5.0,
               "valor":4.5,
               "aptPendente":true,
               "habilitada":true,
               "requerApontamento":true,
               "valorForaFaixa":false,
               "valorForaAlvo":false,
               "apontamentoAtributo":false,
               "apontamentoCalculado":true
            },
            {
               "idcInfDimensional":32,
               "tag":9,
               "descricao":"DiametroSeloOvalizacao",
               "unidade":"0,01 mm",
               "resolucao":1.0,
               "limiteInferior":0.0,
               "alvoInferior":null,
               "alvoSuperior":null,
               "limiteSuperior":9.0,
               "valor":7.2,
               "aptPendente":true,
               "habilitada":true,
               "requerApontamento":true,
               "valorForaFaixa":false,
               "valorForaAlvo":false,
               "apontamentoAtributo":false,
               "apontamentoCalculado":true
            }
         ],
         "infVisuais":[
            {
               "idcInfVisual":1,
               "tag":1,
               "descricao":"Visual",
               "valores":[
                  "OK",
                  "REJ"
               ],
               "valor":"OK",
               "habilitada":true,
               "reprovada":false
            },
            {
               "idcInfVisual":2,
               "tag":2,
               "descricao":"PenteRosca ",
               "valores":[
                  "OK",
                  "REJ"
               ],
               "valor":"OK",
               "habilitada":true,
               "reprovada":false
            }
         ]
      }
   ]
}
 ````
* **Sample Call:**
````markdown
            $http.post('/api/cr/apontamento/requisitadados/MSC1', dados)
               .success(function (data) {
               console.log(data);
              })
            // dados é o objeto mostrado no campo "Parâmetros de Dados"
````

### Estrutura de dados da mensagem de dados de apontamento

A estutura de dados é a mesma para recebimento e evio de dados de qualidade. A seguir é descrito para cada campo da mensagem, o tipo e a descrição de seu objetivo / finalidade.

* **Campos Gerais:**

|Campo        | Tipo                                                | Descrição | Exemplo |
|----------------------| -------------------------------------------------- | ------ | ------ |
| **idPostoTrabalho**   |  string  |  Identificador do Posto de Trabalho em que o inspetor está logado | "ZL1"  |
| **idDispositivo**     |  string  | Identificador único do dispositivo  | "DISP001" |
| **matriculaInspetor** |  string  |  Matrícula do inspetor logado no shalyn  | "705521" |
| **tipoRosca**         |  string  |  Nome da rosca em que está sendo realizado a inspeção | "VAM TOP" | 
| **laudosPossiveis**   | string[] |  Laudos possíveis para apontamento do tubo   | ["OK", "REJ"]
| **valoresAtributo**   | object[] |  Valores constantes para apontamento de inf. dimensional por atributo. Por Exemplo OK/REJ  | [{valor: 1, descricao: "OK"}] |
| valor                 | integer  |  Valor constante p/ identificador numérico p/ apontamentos por atributo  | 1 |
| descricao             |  string  |  Valor constante p/ apontamentos por atributo para ser exibido na | "OK" |
| **pendenciasGerais**  | string[] | Notificações importantes para alerta ao operador. **Caso exista valor sugere-se <br/>mostrar de forma clara ao inspetor ao receber os dados | ["Tubo não se encontra no posto"] |
| **dadosApontamento**  | object[] |  Lista de apontamentos de todos os tubos presentes no posto de trabalho. Detalhes  mostrados a seguir ||

* **Dados de Apontamentos (dadosApontamento):**

|Campo                    | Tipo     | Descrição                                                                    | Exemplo       |
|-------------------------| ---------|----------------------------------------------------------------------------- | ------------- |
| **ippn**                | longint  | Ippn do tubo no posto de trabalho                                            | 123456123456  |
| **laudo**               | string   | Laudo do apontamento do tubo no posto                                        |     "OK"      |
| **codigoDefeito**       | string   | Código de defeito que deve ser preenchido em caso de laudo "REJ"             |    "2122"     |
| **apontamentoPendente** |   bool   | Indicador que tubo possui apontamento pendente                               |      true     | 
| **infDimPendente**      |   bool   | Indicador que pelo menos uma inf. dimensional está pendente de apontamento   |      true     |
| **infVisPendente**      |   bool   | Indicador que pelo menos uma inf. visual está pendente de apontamento        |      true     | 
| **infDimForaFaixa**     |   bool   | Indicador que pelo menos uma inf. dimensional possui valor fora da faixa     |      true     | 
| **infDimForaAlvo**      |   bool   | Indicador que pelo menos uma inf. dimensional possui valor fora do alvo      |      true     | 
| **pendencias**          | string[] | Lista de pendências do apontamento do tubo no posto de trabalho              | ["Apontamento XX - Passo Simples fora da faixa de tolerância"] |
| **infDimensionais**     | object[] | Lista de inf. dimensionais a serem apontadas                                 |               |
| **infVisuais**          | object[] | Lista de inf. visuais a serem apontadas                                      |               |
 
 
 

	
* **Informações Dimensionais (infDimensionais):**

|Campo                     | Tipo     | Descrição                                                                    | Exemplo       |
|--------------------------| ---------|----------------------------------------------------------------------------- | ------------- |
| **idcInfDimensional**    | integer  | Identificador único da inf. dimensional a ser apontada                       |      3       |
| **descricao**            | string   | Descrição da inf. dimensional a ser apontada                                 | "Passo Simples" |
| **tag**                  | integer  | Índice que define a ordenação das inf. dimensionais                          |        1      |
| **valor**                | double?  | Valor apontado pelo inspetor. Pode ter valor nulo (Sem apontamento)          |        127.5       |
| **unidade**              | string   | Indicador que apontamento da inf. dimensional está pendente                  |      "mm"     | 
| **resolucao**            |  double  | Resolução a ser aplicada ao apontamento. Não pode ter mais casas decimais que o especificado   | 0.001 |
| **limiteInferior**       |  double? | Valor mínimo aceitável para aprovar o laudo. Pode ser nulo (sem limite)      |      -1.5     | 
| limiteSuperior           |  double? | Valor máximo aceitável para aprovar o laudo. Pode ser nulo (sem limite)      |      1.5     | 
| **alvoInferior**         |  double? | Valor mínimo sugerido p/ apontamento. Pode ser nulo (sem limite)             |      null     |
| **alvoSuperior**         |  double? | Valor máximo sugerido p/ apontamento. Pode ser nulo (sem limite)             |      null     |
| **aptPendente**          |   bool   | Indicador que o apontamento da inf. dimensional possui pendências            |      true     |
| **habilitada**           |   bool   | Indicador que a inf. dimensional está habilitada                             |      true     |
| **requerApontamento**    |   bool   | Indicador que apontamento inf. dimensional é obrigatório                     |      true     |
| **valorForaFaixa**       |   bool   | Indicador que apontamento inf. dimensional está fora da faixa de tolerância para ser aprovado                            |      true     |
| **valorForaAlvo**        |   bool   | Indicador que apontamento inf. dimensional está fora do alvo desejável       |      false     |
| **apontamentoAtributo**  |   bool   | Indicador do tipo de apontamento: true -> Apontamento numérico, false -> Apontamento por atributo (OK / REJ)  |      false     |
| **apontamentoCalculado** |   bool   | Indicador que o valor da inf. dimensional é calculado pelo rastreamento               |      false     |

	
	
 

	
* **Informações Visuais (infVisuais):**
 
 
|Campo                | Tipo     | Descrição                                                       | Exemplo       |
|---------------------| ---------|---------------------------------------------------------------- | ------------- |
| **idcInfVisual**    | integer  | Identificador único da inf. visual a ser apontada               |      1        |
| **descricao**       | string   | Descrição da inf. visual a ser apontada                         | "Visual"      |
| **tag**             | integer  | Índice que define a ordenação das inf. visuais                  |        1      |
| **valor**           | string   | Valor apontado pelo inspetor. Por exemplo OK/REJ                |      "OK      |
| **valores**         | string[] | Valores possíveis para inf. visual                              | ["OK", "REJ"] | 
| **habilitada**      |   bool   | Indicador que inf. visual está habilitada                       |      true     | 
| **reprovada**       |   bool   | Indicador que inf. visual está reprovada                        |      false     | 
		
		
### Formatação dos Controles de Informação Visual
Segue abaixo sugestões para a elaboração dos controles. É dado o formato que é usado no Cliente QTS como referência.

1. Exibição
- Para inf. visuais deve ser sempre exibido para o inspetor:
- ID da inf. visual (_**"idcInfVisual"**_)
- Descrição da inf. visual (_**"descricao"**_)
- Valores possíveis de Apontamento (_**valores**_)
- No QTS é usado controle do tipo "Combobox, permitindo a seleção dos valores
- O combobox ou controle similar deve ser preenchido com os valores presentes no campo _**valores**_ da inf. visual
- Os valores podem mudar de uma inf. visual para outra
- Por default o campo "valor" já vem preenchido com o valor "OK" quando requisitado e não possuir valor (Campo "valor" = null)
- No QTS as informações visuais ficam no cabeçalho (São as primeiras a serem preenchidas/alteradas se necessário)

2. Cores de exibição
- Quando o valor for reprovado (campo "reprovado" = true) a cor de background fica vermelha `RGB(255,0,0)` e a cor da letra preto: `RGB(0,0,0)`
- Quando o valor for aprovado (campo "reprovado" = false) a cor de background fica bramca `RGB(255,255,255)` e a cor da letra preto: `RGB(0,0,0)`


### Formatação dos Controles de Informação Dimensional
Segue abaixo sugestões para a elaboração dos controles. É dado o formato que é usado no Cliente QTS como referência.

1. Exibição
- Para inf. dimensionais deve ser sempre exibido para o inspetor:
- ID da inf. dimensional (_**"idcInfDimensional"**_)
- Descrição da inf. dimensional (_**"descricao"**_)
- Valor Mínimo se existir (_**"limiteInferior"**_)
- Valor Máximo se existir (_"limiteSuperior"_)
- Alvo Mínimo se existir (_**"alvoInferior"**_)
- Alvo Máximo se existir (_**"alvoSuperior"**_)
- Resolução se existir (_**"resolucao"**_)

2. Tipos de apontamento
- O apontamento pode ser realizado de duas maneiras. Se o campo _**"apontamentoAtributo"**_ for **true** o apontamento é realizado em combobox ou similar, respeitando os valores presentes no campo _**"valoresAtributo"**_. Por exemplo se selecionado "OK", de acordo com o campo _**"valoresAtributo"**_ o campo _**"valor"**_ deverá assumir o valor "1". Se for selecionado "REJ" o campo _**"valor"**_ deve ser preenchido com o valor "2".
- Se o campo _**"apontamentoAtributo"**_ for false o apontamento é dimensional e deve ser preenchido um valor numérico

3. Cores de exibição do(s) campo(s) de inf. dimensional se possuir valor apontado
- Valor fora da faixa (_**"limiteInferior"**_ e _**"limiteSuperior"**). Cor de background vermelha: `RGB(255,0,0)`, Cor da letra amarelo: `RGB(255,255,0)`
- Valor fora do alvo (_**"alvoInferior"**_ e _**"alvoSuperior"**). Cor de background amarela: `RGB(255,255,0)`, Cor da letra preto: `RGB(0,0,0)`
- Valor dentro das faixas e inf. dimensional desabilitada (_**"habilitada"**_) ou é cálculo (_**"apontamentoCalculado"**_), cor de fundo cinza `RGB(195,195,195)`, cor da letra preto `RGB(0,0,0)`
- Demais situações: cor de fundo branca `RGB(255,255,255)` e cor da letra preto `RGB(0,0,0)`

4. Cores de exibição do(s) campo(s) de inf. dimensional se **NÃO** possuir valor apontado
- Apontamento Obrigatório (_**"requerApontamento"**). Cor de background verde: `RGB(191,255,0)`, Cor da letra preto: `RGB(0,0,0)`
- Iinf. dimensional desabilitada (_**"habilitada"**_) ou é cálculo (_**"apontamentoCalculado"**_), cor de fundo cinza `RGB(195,195,195)`, cor da letra preto `RGB(0,0,0)`
- Demais situações: cor de fundo branca `RGB(255,255,255)` e cor da letra preto `RGB(0,0,0)`

### Formatação do Laudo e Motivo de Defeito
Segue abaixo sugestões para a elaboração dos controles. É dado o formato que é usado no Cliente QTS como referência.

1. Exibição
- No QTS a parte de laudo aparece com 3 campos (input): Laudo, CodMotivo e DescricaoMotivo
- O laudo é apresentado em combobox e possui as seguintes opções de seleção: string vazia seguido pelos valores presentes em **_laudosPossiveis_**, geralmente ["", "OK", "REJ"]
- O campo DescricaoMotivo é um combobox preenchido da seguinte forma: "CodDefeito - Descricao". Por exemplo: "2585 - RIR_Risco na rosca"
- Uma vez selecionado é salvo no campo **_CodDefeito_** apenas o código do defeito, neste caso "2585"
- O Campo CodMotivo é apenas um "atalho" para seleção do motivo pelo código. Ao digitar o código é selecionado o motivo referente ao código sem precisar manipular o combobox. Os inspetores geralmente decoram os códigos de defeito mais utilizados

### Comandos Necessários para o apontamento
Segue abaixo a lista de comandos necessários para realizar todo o apontamento:
1. Comando de requisitar dados do tubo no posto de trabalho
2. Comando p/ seleção de valor de inf. visual entre os valores disponíveis
3. Comando p/ apontamento dimensional (numérico) de inf. dimensional
4. Comando p/ seleção de apontamento de inf. dimensional do tipo "atributo" entre os valores possíveis
5. Comando p/ limpar um apontamento de inf. dimensional (Deixá-lo nulo)
6. Comando p/ seleção de laudo de apontamento entre os valores possíveis
7. Comando p/ seleção de código de defeito em caso de laudo reprovado
8. Comando p/ enviar apontamento p/ sistema de rastreamento

### Crítica de dados
A seguir são listados as validações que são realizadas pelo sistema de rastreamento. Se algum destes casos puder ser elaborado na aplicação evitaria o **envio de mensagens desnecessárias** que o QTS vai descartar.
- Só é possível enviar laudo "OK" se não possuir nenhum apontamento visual reprovado ou inf. dimensional fora da faixa (Nesse caso só é permitido laudo "REJ")
- Não é possível enviar laudo "OK" com motivo de defeito. Motivo de defeito deve ser preenchido apenas se laudo "REJ"
- Não é possível enviar laudo "REJ" sem motivo de defeito
